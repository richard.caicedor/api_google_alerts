import requests
import json 
 
class AuthSalesforce:

    def __init__(self):
        self.access_token =''
        self.instance_url =''
        self.email =''
        self.password=''
        with open('config.json', 'r') as file: self.config = json.load(file)
 
    def getToken(self):
        try:
            req = requests.post(self.config['url_token'],params= self.config['API-Salesforce'])
            if not req.json().get('error'):
                self.access_token = req.json().get('access_token')
                self.instance_url = req.json().get('instance_url')
            else:
                raise Exception(req.json().get('error_description'))

        except OSError as err:
            print("Error: {0}".format(err))
    
    def getAccountService(self):
        try:
            self.getToken()
            self.config['api_headers']['Authorization'] = 'Bearer %s' % self.access_token
            req = requests.get(self.instance_url+self.config['url_accountService'], params={},headers=self.config['api_headers'])
            if not req.json()[0].get('errorCode'):
                self.email = req.json()[0].get('Account__c')
                self.password = req.json()[0].get('Password__c')
            else:
                raise Exception(req.json()[0].get('message'))

        except OSError as err:
            print("Error: {0}".format(err))
