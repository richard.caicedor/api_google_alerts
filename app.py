from flask import Flask, jsonify, request
from google_alerts import GoogleAlerts
from AuthSalesforce import AuthSalesforce

app = Flask(__name__)

ObjAuth = AuthSalesforce()
ObjAuth.getAccountService() 

ga = GoogleAlerts(ObjAuth.email,ObjAuth.password)
ga.authenticate()

@app.route('/alerts', methods=['GET'])
def getAlerts(): 
    alerts = ga.list()
    return jsonify(alerts)

@app.route('/alerts/<string:monitor_id>', methods=['GET'])
def getAlertbyId(monitor_id):
    alerts = ga.list()
    alertFound = [alert for alert in alerts if alert['monitor_id'] == monitor_id]
    if (len(alertFound)>0):
        return jsonify(alertFound)
    return jsonify({'message': 'Alert not found'})

@app.route('/alerts', methods=['POST'])
def postAddAlert():
    newAlert = ga.create(request.json['term'], {'delivery': 'RSS'})
    return jsonify(newAlert)

@app.route('/alerts/<string:monitor_id>',methods=['PUT'])
def putEditAlert(monitor_id):
    editAlert = ga.modify(monitor_id, {'delivery': 'RSS', 'monitor_match': 'ALL', 'term' : request.json['term']})
    return jsonify(editAlert)

@app.route('/alerts/<string:monitor_id>', methods=['DELETE'])
def deleteAlert(monitor_id):
    ga.delete(monitor_id)
    return jsonify({'message':'SUCCESS'})

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)
